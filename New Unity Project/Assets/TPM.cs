﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPM : MonoBehaviour
{

    public CharacterController controller;
    public Transform cam;
    public Animator anim;

    public float speed = 6f;

    public float tSmooth = 0.1f;

    float turnVelocity;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
      


        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;



        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;

            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnVelocity, tSmooth);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveD = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            controller.Move(moveD.normalized * speed * Time.deltaTime);
        }

        if (Input.GetKey("a")|| Input.GetKey("s")|| Input.GetKey("d")|| Input.GetKey("w"))
        {
            anim.SetBool("IsRunning", true);
        }

        else
        {
            anim.SetBool("IsRunning", false);
        }

      


    }
}
